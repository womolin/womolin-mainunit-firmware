# WomoLIN Protocol

This document describes the protocol used for exchange of data between client devices and the WomoLIN main unit(s).

## History

| Version | Changes                     |
| ------- | --------------------------- |
| v1.0    | First draft of the protocol |

## Physical Layer

The WomoLIN protocol is based on CAN bus with 11-bit message identifiers (CAN 2.0A).

## Software Layer

There are two kinds of messages on the bus:

- state broadcasts
- commands

A virtual device (VD) in the context of the WomoLIN protocol is an endpoint of the WomoLIN network which periodically sends out its state and provides an interface to trigger commands.
Virtual devices are the interface to physical devices and take care of the protocol needed to communicate with the physical device.

```
                      CAN Bus
                        H L
                        │ │     ┌───────────────────┐
                        │ │     │ WomoLIN Main Unit │
┌───────────────┐       │ │     │                   │
│ Client        ├───────┼─┤     │ ┌───────────────┐ │  ┌──────────────────┐
| Display Unit 1|       | ├─────┼─┤ Virtual Device| |  | Physical Device  |
│               │───────┤ |     | | Heater        ├─┼──┤                  │
└───────────────┘       ├─┼─────┼─┤ VD-ID: 0x06   │ │  │ Diesel Heater    │
                        │ │     │ └───────────────┘ │  └──────────────────┘
┌───────────────┐       │ │     │                   │
│ Client        ├───────┼─┤     │ ┌───────────────┐ │  ┌──────────────────┐
│ Display Unit 2│       │ ├─────┼─┤ Virtual Device│ │  │ Physical Device  │
│               ├───────┤ │     │ │ Water Tank    ├─┼──┤                  │
└───────────────┘       ├─┼─────┼─┤ VD-ID: 0x09   │ │  │ Water Tank Sensor│
                        │ │     │ └───────────────┘ │  └──────────────────┘
┌───────────────┐       │ │     │         .         │
│ Client        ├───────┼─┤     │         .         │
│ Tank Monitor  │       │ │     │         .         │
│               ├───────┤ │     │                   │
└───────────────┘       │ │     │                   │
                        │ │     └───────────────────┘
                        │ │
```

_Fig.1: Overview of the WomoLIN protocol architecture_

### Message IDs

Each virtual device (i.e. endpoint) configured on the WomoLIN main unit has a **unique** 8-bit identifier (VD-ID).
This identifier is used by the main unit to broadcast the virtual devices' states and by clients to trigger commands of a virtual device.
The VD-IDs are chosen by the user during configuration of the WomoLIN main unit.
If multiple virtual devices of the same type are configured (e.g. two tank sensors), the IDs must be different.

The CAN bus protocol uses messages with 11-bit message identifiers.
The protocol prioritizes messages with higher message ID.
In order to prioritize command messages on the bus, the message ID for command messages has to be higher than for state broadcast messages.

Therefore, WomoLIN uses CAN bus message identifiers in the following format:

| **Bit (MSB to LSB)** | 10..8                                                                   | 7..0  |
| -------------------- | ----------------------------------------------------------------------- | ----- |
| **Description**      | Message type:<br/>`0` for state broadcasts<br/>`1` for command messages | VD-ID |

**Example:** Using the VD-IDs from _Fig.1_, a state broadcast of the virtual device with name "Heater" would be 0x006, whereas the message ID of a command message targeting the virtual device would be 0x106.

### State Broadcast Messages

The WomoLIN main unit broadcasts the state of each configured virtual device (VD) periodically using state broadcast messages.
A state broadcast message contains a single state of the virtual device.

| Msg identifier  | UnitType (0) | Properties (1) | State id(Path) (2) | Data Bytes (3..7) (value) |
| --------------- | ------------ | -------------- | ------------------ | ------------------------- |
| `0x000` + VD-ID | float        | Writeable      | 0                  | target temperature        |
| `0x000` + VD-ID | float        |                | 1                  | current temperature       |
| `0x000` + VD-ID | int          | Writeable      | 2                  | power level               |
| `0x000` + VD-ID | bool         | Writeable      | 3                  | heat by temp              |
| `0x000` + VD-ID | bool         | Writeable      | 4                  | heat by power             |
| `0x000` + VD-ID | bool         | Writeable      | 5                  | ventilation               |
| `0x000` + VD-ID | bool         | Writeable      | 6                  | boiler                    |
| `0x000` + VD-ID | bool         | Writeable      | 7                  | heat element              |

_Table.1: Example state table for a heater. Each line corresponds to a broadcast message._

#### Mapping to MQTT\*\*

The state broadcasts can easily be translated to MQTT messages by using a mapping for VD-ID and State id to path:

| VD-ID | State id | MQTT path example |
| ----- | -------- | ----------------- |
| 0x01  | 1        | /heater1/current  |
| 0x01  | 2        | /heater1/power    |
| 0x02  | 1        | /light/isOn       |

### Command Messages

If a client wants to trigger a command on a virtual device (VD), it writes a command message on the bus.
The message ids of command messages always have the first three bits of the message identifier set to 0b001 in order to be prioritized and easily recognized by the main unit(s).
The first byte of the data refers to the state id of the virtual device, which is targeted for the change.

If a state id is not known to a targeted virtual device, the command will be dropped.
Otherwise the virtual device's driver will execute the command and update the internal states accordingly.

| **Field**       | Msg Identifier  | State id (0) | Data Bytes (1..n)     |
| --------------- | --------------- | ------------ | --------------------- |
| **Description** | `0x100` + VD-ID | 1            | 5                     |
| **Pos. values** | `0x100..0x1FF`  | `0x04..0xFF` | `0x00..0xFF` for each |

_Table.2: Fields of a command message_

- Requesting state update from virtual device? May be needed if broadcasting frequency is too low. Could be write to ID with no data.
