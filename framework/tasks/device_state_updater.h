#pragma once

#include "device_list.h"
#include "igpio.h"
#include "repeating_runnable.h"

namespace framework
{
/**
 * Manager thread taking care of scheduling state updates of the configured
 * devices.
 */
class DeviceStateUpdater : RepeatingRunnable
{
    IGPIO& led_;
    const DeviceList* devices_;
    uint32_t minimum_desired_ms_;

    uint32_t calculateMinimumDesiredMs();

  public:
    DeviceStateUpdater(const IGPIO& led, const DeviceList* devices);
    virtual ~DeviceStateUpdater();
    void runRepeatedly() override;
    
    DeviceStateUpdater() = delete;
    DeviceStateUpdater(const DeviceStateUpdater&) = delete;
};

} // namespace framework
