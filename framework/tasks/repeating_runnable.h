#pragma once

#include "irunnable.h"

namespace framework
{
/**
 * Runnable containing a loop inside the run method. The loop will be repeated until the execution
 * is interrupted. Interruption is needed to stop the threads for testing.
 */
class RepeatingRunnable : IRunnable
{
    bool interruped_ = false;

  public:
    RepeatingRunnable();
    ~RepeatingRunnable();
    
    void run() override;

    /** Stops the execution as soon as possible. */
    void interrupt();

    /** Returns whether the execution of this runnable has been interrupted and will stop soon. */
    bool isInterrupted();

    /**
     * Runs the code repeatedly, until interupt() is called.
     * Implementors can use isInterrupted to check (e.g. during loops), whether execution should be stopped and return early.
     */
    virtual void runRepeatedly() = 0;
};
} // namespace framework
