#pragma once

#include "device_list.h"
#include "icontrol_bus.h"
#include "repeating_runnable.h"
#include "time_source.h"

namespace framework
{
/**
 * Worker coordinating the transmission of state broadcast messages and
 * reception of commands on the control bus.
 */
class ControlBusWorker : RepeatingRunnable
{
  public:
    ControlBusWorker(IControlBus* control_bus, DeviceList* devices);
    ~ControlBusWorker();

    ControlBusWorker() = delete;
    ControlBusWorker(const ControlBusWorker&) = delete;

    void runRepeatedly() override;

  private:
    const uint16_t kStateBroadcastIntervalMs = 1000;
    IControlBus* control_bus_;
    DeviceList* devices_;
    StateContainer* current_state_;
    Command current_command_;
    uint32_t start_time_ = 0;
    int32_t available_sleep_time_ = 0;
    bool interrupted_ = false;

    void sendDeviceStates(const VirtualDeviceBase* virtual_device, VirtualDeviceIdentifier vdid) const;
    void handleIncomingCommands();
};

} // namespace framework