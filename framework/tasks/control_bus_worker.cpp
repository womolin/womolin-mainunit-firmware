#include "control_bus_worker.h"

namespace framework
{
ControlBusWorker::ControlBusWorker(IControlBus* control_bus, DeviceList* devices)
    : control_bus_(control_bus), devices_(devices)
{
    // nothing to do
}

ControlBusWorker::~ControlBusWorker()
{
    // nothing to do
}

void ControlBusWorker::runRepeatedly()
{
    start_time_ = millis();

    handleIncomingCommands();

    devices_->forEach([this](const auto device_entry) {
        sendDeviceStates(device_entry->virtual_device, device_entry->vdid);
    });

    // make sure to wait and not flood the bus
    available_sleep_time_ = kStateBroadcastIntervalMs - (millis() - start_time_);
    if(available_sleep_time_ > 0) {
        delayMs(available_sleep_time_);
    }
}

void ControlBusWorker::sendDeviceStates(const VirtualDeviceBase* virtual_device, VirtualDeviceIdentifier vdid) const
{
    const StateContainer& state_container = virtual_device->getCurrentState();
    if(state_container.hasError()) {
        control_bus_->writeBroadcast(vdid, state_container.getError());
    } else {
        for(auto& entry : state_container.getStates()) {
            control_bus_->writeBroadcast(vdid, entry);
        }
    }
}

void ControlBusWorker::handleIncomingCommands()
{
    while (control_bus_->popCommandMessageFromInbox(current_command_)) {
        // we got a command, hand it over to the device
        devices_->get(current_command_.vdid)->triggerCommand(current_command_);
    }
}

} // namespace framework