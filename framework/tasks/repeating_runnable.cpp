#include "repeating_runnable.h"

namespace framework
{
void RepeatingRunnable::run()
{
    while(!interruped_) {
        runRepeatedly();
    }
}

void RepeatingRunnable::interrupt()
{
    interruped_ = true;
}
bool RepeatingRunnable::isInterrupted()
{
    return interruped_;
}
} // namespace framework
