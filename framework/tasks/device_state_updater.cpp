#include <device_state_updater.h>

namespace framework
{
DeviceStateUpdater::DeviceStateUpdater(const IGPIO& led, const DeviceList* devices) // TODO: ref vs pointer?
    : led_(const_cast<IGPIO&>(led)), devices_(devices)
{
}

void DeviceStateUpdater::runRepeatedly()
{
    uint32_t start = millis();
    led_.setLow();
    devices_->forEach([](auto device_entry) { device_entry->virtual_device->triggerUpdate(); });
    led_.setHigh();

    if(millis() - start < minimum_desired_ms_) {
        // TODO: go to sleep?
    } else {
        // TODO: show warning?
    }
}
} // namespace framework
