namespace framework
{
/** The Runnable interface should be implemented by any class whose instances are intended to be executed by a thread. */
class IRunnable
{
  public:
    virtual ~IRunnable(){};
    virtual void run() = 0;
};

}