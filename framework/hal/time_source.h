#pragma once

#include <stdint.h>

namespace framework
{
/** Source of time. Returns the time passed since startup in milliseconds. */
uint32_t millis();

/** Delays (blocking) code execution by the given amount of milliseconds. */
void delayMs(uint32_t milliseconds);
} // namespace framework
