#pragma once

#include <stdbool.h>
#include <stdint.h>

#include "command.h"
#include "unit_types.h"

namespace framework
{
/**
 * Interface handling write and read of messages on the control bus in the WomoLIN
 * network.
 */
class IControlBus
{
  public:
    virtual ~IControlBus();

    /**
     * Writes a broadcast message with the given entry on the bus.
     *
     * @param vdid The virtual device id. This method will extend the id to 11 bit.
     * @param entry The unit to write.
     */
    void writeBroadcast(uint8_t vdid, const UnitEntryBase::ptr entry);

    /**
     * Reads the next message from the inbox and writes it into the given buffer.
     *
     * @param entry The destination, where the command will be written to
     * @return True, if a command was written into the buffer, false if no command is available.
     */
    bool popCommandMessageFromInbox(Command& command);
};

} // namespace framework