#include "virtual_device_base.h"

namespace framework
{
VirtualDeviceBase::VirtualDeviceBase(uint8_t virtualDeviceId, StateContainer& state, uint16_t updateInterval = 1000)
    : kVirtualDeviceId(virtualDeviceId), kUpdateInterval(updateInterval), state_(state)
{
    // nothing to do
}

const StateContainer& VirtualDeviceBase::getCurrentState() const
{
    return state_;
}

uint16_t VirtualDeviceBase::getUpdateInterval() const
{
    return kUpdateInterval;
}

void VirtualDeviceBase::triggerUpdate()
{
    if(millis() > last_update_ + kUpdateInterval) {
        update();
        last_update_ = millis();
    }
}

bool VirtualDeviceBase::hasError(void) const
{
    return state_.hasError();
}

} // namespace framework
