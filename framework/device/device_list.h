#pragma once

#include <stdint.h>

#include <functional>

#include "virtual_device_base.h"

namespace framework
{
/**
 * Virtual device identifier used to identify a device configured on the WomoLIN
 * board. Has to be unique across all devices.
 */
using VirtualDeviceIdentifier = uint8_t;

/**
 * Entry in the map of devices.
 */
typedef struct {
    VirtualDeviceIdentifier vdid;
    VirtualDeviceBase* virtual_device;
} DeviceListEntry;

/**
 * Simple map to store the configured virtual devices.
 */
class DeviceList
{
    DeviceListEntry* buffer_;
    uint32_t total_size_;
    uint32_t first_free_element_index_ = 0;
    uint32_t longest_update_interval_ = 0;

  public:
    DeviceList(int size) : buffer_(new DeviceListEntry[size]), total_size_(size)
    {
        // nothing to do
    }

    ~DeviceList()
    {
        delete[] buffer_;
    }

    DeviceList() = delete;
    DeviceList(const DeviceList& device_list) = delete;

    /** Adds a virtual device with the given identifier to the list. */
    void put(VirtualDeviceIdentifier vdid, VirtualDeviceBase* virtual_device)
    {
        if(first_free_element_index_ == total_size_) {
            return; // can not add any more elements
        }

        buffer_[first_free_element_index_].vdid = vdid;
        buffer_[first_free_element_index_].virtual_device = virtual_device;

        const uint32_t update_interval = virtual_device->getUpdateInterval();
        if(update_interval > longest_update_interval_) {
            longest_update_interval_ = update_interval;
        }

        first_free_element_index_++;
    }

    /**
     * Fetches the virtual device with the given identifier from the list.
     * If no device with the identifier can be found, nullptr is returned.
     */
    VirtualDeviceBase* get(VirtualDeviceIdentifier vdid) const
    {
        for(uint32_t i = 0; i <= first_free_element_index_; i++) {
            if(buffer_[i].vdid == vdid) {
                return buffer_[i].virtual_device;
            }
        }
        return nullptr;
    }

    void forEach(const std::function<void(const DeviceListEntry*)>& delegate) const
    {
        for(uint32_t i = 0; i < total_size_; i++) {
            delegate(&buffer_[i]);
        }
    }

    uint32_t getLongestUpdateInterval() const
    {
        return longest_update_interval_;
    }
};

} // namespace framework