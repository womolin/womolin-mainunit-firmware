#pragma once

#include <stdint.h>

#include "command.h"
#include "state_container.h"
#include "time_source.h"
#include "unit_types.h"

namespace framework
{
/**
 * Devices are single external peripherals connected to the WomoLIN board.
 * This interface offers a generic way to get the current state of the device
 * and available commands which can be sent to the device. A specific device
 * type can be registered multiple times in the WomoLIN registry (e.g. two
 * separate temperature sensors)
 */
class VirtualDeviceBase
{
    /** Unique virtual device id */
    const uint8_t kVirtualDeviceId;

    /** Desired minimum time between two update calls. */
    const uint16_t kUpdateInterval;

    /** Container with all units the device offers to clients. */
    StateContainer state_;

    /** Tick number the last update call finished. */
    uint32_t last_update_ = 0;

  protected:
    /**
     * Base constructor for virtual devices. The state object and commands should
     * already be allocated. commandLength corresponds to the number of commands
     *
     * @param virtualDeviceId The unique id of the virtual device
     * @param state The vector of unit entries defining the state of the virtual
     * device
     * @param updateInterval The minimum time (ms) between two update() calls
     */
    VirtualDeviceBase(uint8_t virtualDeviceId, StateContainer& state, uint16_t updateInterval);

    /**
     * In this method, the virtual device fetches all relevant data from the
     * physical device and updates the states object. Should be implemented as
     * fast as possible, since this method is blocking.
     *
     * If an error occurs during the update, the error code in the state container should be set.
     */
    virtual void update() = 0;

  public:
    virtual ~VirtualDeviceBase() = default;
    VirtualDeviceBase() = delete;
    VirtualDeviceBase(const VirtualDeviceBase&) = delete;

    /**
     * Called, when the device is requested to update its state.
     * If errors occurred during updates, the error code in the state object
     * should be used for signaling.
     */
    const StateContainer& getCurrentState() const;

    /** Returns the desired update interval of this virtual device. */
    uint16_t getUpdateInterval() const;

    /**
     * Triggers the update call of this virtual device, if applicable i.e.
     * since the last call more than the desired update interval has passed.
     */
    void triggerUpdate();

    /**
     * Called when a new command for this device (matching the vdid) was received.
     * Drivers implementing this interface need to make sure to copy needed informations of the command to their internal datastructures.
     */
    virtual void triggerCommand(Command& command) = 0;

    bool hasError(void) const;
};
} // namespace framework
