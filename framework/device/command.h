#pragma once

namespace framework
{
/** A command describes a request of a client to change a specific unit of a state. */
typedef struct {
    /** Id of the targeted virtual device. */
    uint8_t vdid;
    /** Id of the unit which is written. */
    uint8_t id;
    /** New value which should be written. */
    uint8_t value[4]; // TODO: Better way to deliver the value? Copy into buffer? Dangling pointer!
} Command;
} // namespace framework