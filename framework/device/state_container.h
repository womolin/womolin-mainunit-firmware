#pragma once

#include <vector>
#include "unit_types.h"

namespace framework
{

class StateContainer {
public:
    typedef std::vector<UnitEntryBase::ptr> UnitVector;
protected:
    ut_ecode::ptr error_code_;
    UnitVector states_;
public:
    StateContainer(void) = delete;
    StateContainer(ut_ecode::ptr error_code);
    StateContainer(const StateContainer& other);
    StateContainer(StateContainer&& other);
    
    //void AddState(UnitEntryBase& unit_entry);

    bool hasError(void) const
    {
        return error_code_->hasError();
    }

    const ut_ecode::ptr getError(void) const
    {
        return error_code_;
    }

    const UnitVector& getStates(void) const
    {
        return states_;
    }
};

} // namespace framework