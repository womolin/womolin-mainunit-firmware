# include "state_container.h"

using namespace framework;

StateContainer::StateContainer(const StateContainer& other)
:error_code_(other.error_code_)
,states_(other.states_)
{}

StateContainer::StateContainer(StateContainer&& other)
:error_code_(std::move(other.error_code_))
,states_(std::move(other.states_))
{}

StateContainer::StateContainer(ut_ecode::ptr error_code)
:error_code_(error_code)
{}
