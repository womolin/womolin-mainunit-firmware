#pragma once
// ------------------------------------------------------------------------------------------------
#include <cstdint>
#include <cstring>
#include <type_traits>
#include <memory>
// ------------------------------------------------------------------------------------------------
namespace framework
{
// ------------------------------------------------------------------------------------------------

/** Defines the properties for a unit entry. */
struct PropertyBits {
    bool writeable : 1;
};

typedef union _Properties {
    PropertyBits properties;
    uint8_t byteval = 0;
} Properties;

static_assert(sizeof(Properties) == 1, "Error: size of Properties must not exceed 1 byte!");

// ------------------------------------------------------------------------------------------------

typedef char ErrorCode;

// ------------------------------------------------------------------------------------------------
/// Available unit types.
enum UnitType : uint8_t {
    kUtErrorCode = 127,
    kUtBool = 1, /// bool
    kUtChar, /// int8_t
    kUtByte, /// uint8_t
    kUtInt16,
    kUtUnsignedInt16,
    kUtInt32,
    kUtUnsignedInt32,
    kUtFloat
};
// ------------------------------------------------------------------------------------------------
/** Helper template to determine the UnitType() of a scalar C type
 */
template <typename T> struct UnitTypeInfoT {
    /// UnitType() of \a T
    static const UnitType unit_type;

    /// Returns the unit_type() corresponding to \a T as uint8_t.
    static constexpr uint8_t getTypeId(void)
    {
        return (uint8_t)unit_type;
    }
};
// ------------------------------------------------------------------------------------------------
class UnitEntryBase
{
  public:
    typedef std::shared_ptr<UnitEntryBase> ptr;

    /**
     * Writes the unit entry into a given \a buffer with a
     * size of at least \a buffer_size.
     *
     * @return The number of bytes written into the buffer.
     */
    virtual uint32_t serialize(uint8_t* buffer, uint32_t buffer_size) const = 0;
    virtual ~UnitEntryBase(void) {}

    static ptr deserialize(uint8_t* buffer, uint32_t buffer_size);

};
// ------------------------------------------------------------------------------------------------
/**
 * A unit entry or short unit in the context of WomoLIN is a state value delivered by a virtual
 * device. A unit has a specific type (see EUnitType) as well as properties used for signaling
 * clients. The id of a unit is used to map a unit back to the specific state of the virtual device.
 * Commands are using the id to target a specific state of the virtual device.
 * Each broadcast message sent by the WomoLIN is containing one unit.
 */
template <typename T> class UnitEntryT : public UnitEntryBase
{
  public:
    typedef T value_type;
    typedef UnitTypeInfoT<value_type> unit_info_type;
    typedef UnitEntryT<value_type> my_type;
    typedef std::shared_ptr<my_type> ptr;
  protected:
    value_type value_;
    uint8_t id_;
    Properties properties_;

    /// Creates a new instance, all params are stored into the corresponding
    /// members.
    UnitEntryT(value_type value, uint8_t id, const Properties& properties = Properties())
        : value_(value), id_(id), properties_(properties)
    {}

    explicit UnitEntryT(uint8_t* buffer, uint32_t buffer_size)
    {
        // TODO: ASSERT(buffer[0] == getTypeId());
        // TODO: ASSERT(buffer_size >= (3 + valueSize()));
        properties_.byteval = buffer[1];
        id_ = buffer[2];
        memcpy(&value_, &buffer[3], valueSize());
    }

  public:
    static ptr create(value_type value, uint8_t id, const Properties& properties = Properties())
    {
      return ptr(new my_type(value, id, properties));
    }

    static ptr fromBuffer(uint8_t* buffer, uint32_t buffer_size)
    {
      return ptr(new my_type(buffer, buffer_size));
    }

    constexpr uint8_t getTypeId(void) const
    {
        return unit_info_type::getTypeId();
    }

    constexpr uint32_t valueSize(void) const
    {
        return sizeof(value_type);
    }

    uint32_t serialize(uint8_t* buffer, uint32_t buffer_size) const
    {
        // TODO: ASSERT(buffer_size >= (3 + valueSize()));
        buffer[0] = getTypeId();
        buffer[1] = properties_.byteval;
        buffer[2] = id_;
        memcpy(&buffer[3], &value_, valueSize());
        return 3 + valueSize();
    }

    value_type getValue(void) const
    {
        return value_;
    }

    uint8_t getID(void) const
    {
        return id_;
    }

    bool operator==(const my_type& other) const
    {
        return (value_ == other.value_) && (id_ == other.id_) &&
               (properties_.byteval == other.properties_.byteval);
    }

    /// Deserializes an instance of this type from \a buffer and its \a buffer_size and returns
    /// a pointer to it.
    /// Returns \c nullptr in case of error.
    /// \sa UnitEntryBase::deserialize
    static ptr deserialize(uint8_t* buffer, uint32_t buffer_size)
    {
        if(buffer_size < (sizeof(value_type) + 3)) return nullptr;
        return std::dynamic_pointer_cast<my_type>(UnitEntryBase::deserialize(buffer, buffer_size));
    }

    /// Returns \c true if this instance contains an error.
    /// only valid for T == ErrorCode.
    bool hasError(void) const;

    /// Returns the errorCode carried by this instance
    /// Only valid for T == ErrorCode.
    uint8_t errorCode(void) const;
};
// ------------------------------------------------------------------------------------------------
typedef UnitEntryT<bool> ut_bool;
typedef UnitEntryT<int8_t> ut_int8;
typedef UnitEntryT<uint8_t> ut_uint8;
typedef UnitEntryT<int16_t> ut_int16;
typedef UnitEntryT<uint16_t> ut_uint16;
typedef UnitEntryT<int32_t> ut_int32;
typedef UnitEntryT<uint32_t> ut_uint32;
typedef UnitEntryT<float> ut_float;
typedef UnitEntryT<double> ut_double;
typedef UnitEntryT<ErrorCode> ut_ecode;
// ------------------------------------------------------------------------------------------------

} // namespace framework