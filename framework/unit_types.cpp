#include "unit_types.h"
// ------------------------------------------------------------------------------------------------
using namespace framework;
// ------------------------------------------------------------------------------------------------
template <> const UnitType UnitTypeInfoT<bool>::unit_type = kUtBool;
template <> const UnitType UnitTypeInfoT<int8_t>::unit_type = kUtChar;
template <> const UnitType UnitTypeInfoT<uint8_t>::unit_type = kUtByte;
template <> const UnitType UnitTypeInfoT<int16_t>::unit_type = kUtInt16;
template <> const UnitType UnitTypeInfoT<uint16_t>::unit_type = kUtUnsignedInt16;
template <> const UnitType UnitTypeInfoT<int32_t>::unit_type = kUtInt32;
template <> const UnitType UnitTypeInfoT<uint32_t>::unit_type = kUtUnsignedInt32;
template <> const UnitType UnitTypeInfoT<float>::unit_type = kUtFloat;
template <> const UnitType UnitTypeInfoT<ErrorCode>::unit_type = kUtErrorCode;
// ------------------------------------------------------------------------------------------------
UnitEntryBase::ptr UnitEntryBase::deserialize(uint8_t* buffer, uint32_t buffer_size)
{
    UnitEntryBase* pUnit = nullptr;
    switch(buffer[0]) {
    case kUtBool:
        return ut_bool::fromBuffer(buffer, buffer_size);
    case kUtChar:
        return ut_int8::fromBuffer(buffer, buffer_size);
    case kUtByte:
        return ut_uint8::fromBuffer(buffer, buffer_size);
    case kUtInt16:
        return ut_int16::fromBuffer(buffer, buffer_size);
    case kUtUnsignedInt16:
        return ut_uint16::fromBuffer(buffer, buffer_size);
    case kUtInt32:
        return ut_int32::fromBuffer(buffer, buffer_size);
    case kUtUnsignedInt32:
        return ut_uint32::fromBuffer(buffer, buffer_size);
    case kUtFloat:
        return ut_float::fromBuffer(buffer, buffer_size);
    case kUtErrorCode:
        return ut_ecode::fromBuffer(buffer, buffer_size);
    default:
        // TODO: ASSERT(0)
        break;
    }
    // TBD
    return ptr();
}
// ------------------------------------------------------------------------------------------------
template <>
bool UnitEntryT<ErrorCode>::hasError(void) const
{
    return value_ != 0;
}
// ------------------------------------------------------------------------------------------------
template <>
uint8_t UnitEntryT<ErrorCode>::errorCode(void) const
{
    return (uint8_t)value_;
}
// ------------------------------------------------------------------------------------------------
