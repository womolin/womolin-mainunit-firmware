# Coding Guidelines for the WomoLIN Main Unit Firmware

This document lists coding guidelines for the WomoLIN main unit firmware.
Please try to adhere to these rules.

The guidelines are based on https://isocpp.github.io/CppCoreGuidelines/CppCoreGuidelines

These guidelines are open for improvement. To suggest a change, please open a ticket.

## Naming Conventions

Consistent namings are crutial for high readability. Because of that, we follow the naming conventions listed as follows.

The following naming conventions are based on the [Google C++ Style Guide](https://google.github.io/styleguide/cppguide.html#General_Naming_Rules).
Use it as fallback if you can not find a matching rule here.

### File Names

File names are based on the class or struct they define. For a class named `MyUsefulClass` the filenames would be as follows:

```
my_useful_class.h
my_useful_class.cpp
```

Use one file per class/struct.

### Type Names

Consistent with [Google C++ Style Guide](https://google.github.io/styleguide/cppguide.html#Type_Names):  
Type names start with a capital letter and have a capital letter for each new word, with no underscores: MyExcitingClass, MyExcitingEnum.

### Variable Names

- Variable names, public fields and struct data members are written lowercase with underscore: `my_var` or `my_public_class_member`
- For `private` or `protected` fields of classes, add a trailing underscore: `my_private_class_member_`

### Constants

Consistent with [Google C++ Style Guide](https://google.github.io/styleguide/cppguide.html#Constant_Names):  
Variables declared constexpr or const, and whose value is fixed for the duration of the program, are named with a leading "k" followed by mixed case.
Underscores can be used as separators in the rare cases where capitalization cannot be used for separation. For example:

```
const int kDaysInAWeek = 7;
const int kAndroid8_0_0 = 24;  // Android 8.0.0
```

### Function Names

Functions should start lower-case and have a capital letter for each new word.

```
addTableEntry()
deleteUrl()
openFileOrDie()
```

## Comments/Documentation

- Write inline comments for code that is hard to understand without it.
- Write interface documentation (Doxygen-style) for classes, structs, public methods and public fields. This helps to clearly state the concern of a class/method and describes how to use it without having to look on the implementation side.

## Code Style
* Use `#pragma once` instead of define guards.
* Use the provided [`.clang-format`](.clang-format) to autoformat the code before commiting.