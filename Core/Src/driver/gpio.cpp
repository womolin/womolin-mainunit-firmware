/* SPDX-License-Identifier: GPLv3-or-later */
/* Copyright (c) 2019 Project WomoLIN */
/* Author Tilo Seeck <tiloseeck@web.de> */

#include "gpio.h"

namespace mainunit::driver {

GPIO::GPIO(uint16_t pin, GPIO_TypeDef *port)
    : m_pin(pin), m_port(port), m_state(false) {}

GPIO::~GPIO() {}

void GPIO::setHigh() {
  m_port->BSRR = (uint32_t)m_pin;
  m_state = true;
}

void GPIO::setLow() {
  m_port->BRR = (uint32_t)m_pin;
  m_state = false;
}

void GPIO::toggle() {
  if (m_state) {
    setLow();
  } else {
    setHigh();
  }
}

bool GPIO::getState() {
  if ((m_port->IDR & m_pin) != 0x00u) {
    m_state = true;
  } else {
    m_state = false;
  }

  return m_state;
}

}  // namespace mainunit::driver
