# WomoLIN Firmware for the Main Unit

This repository contains the firmware for the WomoLIN main unit.

## Build
To build, make sure to dowload the [GNU Arm Embedded Toolchain](https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm/downloads).

Remind the path where the toolchain is stored (arm-none-eabi) and either add to your env PATH or
pass it directly to cmake like in the following example:

```
$ mkdir build
$ cd build
$ cmake .. -DCOMPILER_PATH=<toolchain-path>
$ make
```

You can now flash the binary in the build directory.

## Test
Tests are based on the [Catch2-Framework](https://github.com/catchorg/Catch2) and can be build using your local linux compiler (clang, gcc).
To build and run the tests specify the target to cmake, make it and execute like in the following example:

```
$ mkdir build
$ cd build
$ cmake .. -DTARGET=test
$ make
$ test/tests

===============================================================================
All tests passed (139 assertions in 10 test cases)
```
