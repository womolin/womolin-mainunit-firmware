# define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
# include <catch2/catch.hpp>

// empty. All test cases should be in separate cpp source files.