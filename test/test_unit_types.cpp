#include <catch2/catch.hpp>

#include "unit_types.h"

using namespace framework;
// ------------------------------------------------------------------------------------------------
TEST_CASE("Property class", "[Properties]")
{
    Properties props = Properties();

    SECTION("initial behavior")
    {
        REQUIRE(props.properties.writeable == false);
        REQUIRE(props.byteval == 0);
    }

    SECTION("writable field")
    {
        props.properties.writeable = true;
        REQUIRE(props.properties.writeable == true);
        REQUIRE(props.byteval == 0b00000001);
    }

    SECTION("copy construction")
    {
        props = Properties{ .properties{ .writeable = true } };
        REQUIRE(props.properties.writeable == true);

        props = Properties{ .properties{ .writeable = false } };
        REQUIRE(props.properties.writeable == false);
    }
}
// ------------------------------------------------------------------------------------------------
TEST_CASE("ut_ecode", "[UnitEntryT<ErrorCode>]")
{
    const uint32_t buffer_size = 10;
    uint8_t buffer[buffer_size];
    uint32_t written_bytes = 0;
    ut_ecode::ptr pVal;
    ut_ecode::ptr ec = ut_ecode::create((uint8_t)0, 1);
    REQUIRE(ec->hasError() == false);

    ec = ut_ecode::create(1, 2);
    REQUIRE(ec->hasError() == true);

    written_bytes = ec->serialize(buffer, buffer_size);
    REQUIRE(written_bytes == 4);
    REQUIRE(buffer[0] == kUtErrorCode);
    REQUIRE(buffer[1] == (Properties{ .properties{ .writeable = false } }).byteval);
    REQUIRE(buffer[2] == 2);
    REQUIRE(buffer[3] == 1);

    pVal = ut_ecode::deserialize(buffer, written_bytes);
    REQUIRE((bool)pVal);
    REQUIRE(pVal->hasError() == true);
    REQUIRE(pVal->getID() == 2);

    REQUIRE(*pVal == *ec);

    pVal = ut_ecode::deserialize(buffer, written_bytes - 1);
    REQUIRE(!pVal);

    buffer[0] = kUtBool;
    pVal = ut_ecode::deserialize(buffer, written_bytes);
    REQUIRE(!pVal);
}
// ------------------------------------------------------------------------------------------------
template <typename T> struct TestSetupT {
    static T valueA, valueB;
    static uint8_t id;
};

template <> bool TestSetupT<bool>::valueA = false;
template <> bool TestSetupT<bool>::valueB = true;
template <> uint8_t TestSetupT<bool>::id = 4;

template <> int8_t TestSetupT<int8_t>::valueA = -100;
template <> int8_t TestSetupT<int8_t>::valueB = 100;
template <> uint8_t TestSetupT<int8_t>::id = 6;

template <> uint8_t TestSetupT<uint8_t>::valueA = 254;
template <> uint8_t TestSetupT<uint8_t>::valueB = 1;
template <> uint8_t TestSetupT<uint8_t>::id = 8;

template <> int16_t TestSetupT<int16_t>::valueA = -10000;
template <> int16_t TestSetupT<int16_t>::valueB = 10000;
template <> uint8_t TestSetupT<int16_t>::id = 10;

template <> uint16_t TestSetupT<uint16_t>::valueA = 0xFFFE;
template <> uint16_t TestSetupT<uint16_t>::valueB = 1;
template <> uint8_t TestSetupT<uint16_t>::id = 12;

template <> int32_t TestSetupT<int32_t>::valueA = -1000000000;
template <> int32_t TestSetupT<int32_t>::valueB = 1000000000;
template <> uint8_t TestSetupT<int32_t>::id = 14;

template <> uint32_t TestSetupT<uint32_t>::valueA = 0xFFFFFFFE;
template <> uint32_t TestSetupT<uint32_t>::valueB = 1;
template <> uint8_t TestSetupT<uint32_t>::id = 16;

template <> float TestSetupT<float>::valueA = -12345.6789f;
template <> float TestSetupT<float>::valueB = 98765.4321f;
template <> uint8_t TestSetupT<float>::id = 18;
// ------------------------------------------------------------------------------------------------
TEMPLATE_TEST_CASE("UnitEntryT", "[UnitEntryT][template]", bool, int8_t, uint8_t, int16_t, uint16_t, int32_t, uint32_t, float)
{
    typedef UnitEntryT < TestType > test_type;
    typedef TestSetupT<TestType> test_setup;

    const uint8_t test_id = test_setup::id;
    TestType valueA = test_setup::valueA, valueB = test_setup::valueB;

    const uint32_t buffer_size = 10;
    uint8_t buffer[buffer_size];
    uint32_t written_bytes = 0;
    typename test_type::ptr pVal;

    typename test_type::ptr val = test_type::create(valueA, test_id - 1, Properties());

    REQUIRE(val->getValue() == valueA);
    REQUIRE(val->getID() == test_id - 1);

    val = test_type::create(valueB, test_id, Properties());

    REQUIRE(val->getValue() == valueB);
    REQUIRE(val->getID() == test_id);

    written_bytes = val->serialize(buffer, buffer_size);
    REQUIRE(written_bytes == 3 + sizeof(TestType));
    REQUIRE(buffer[0] == test_type::unit_info_type::getTypeId());
    REQUIRE(buffer[1] == 0);
    REQUIRE(buffer[2] == test_id); // id_ == test_id
    REQUIRE(*(TestType*)&buffer[3] == valueB); // value == valueB

    pVal = test_type::deserialize(buffer, written_bytes);
    REQUIRE((bool)pVal);
    REQUIRE(pVal->getID() == test_id);
    REQUIRE(pVal->getValue() == valueB);

    REQUIRE(*pVal == *val);

    pVal = test_type::deserialize(buffer, written_bytes - 1);
    REQUIRE(!pVal);

    buffer[0] = 0xFF;
    pVal = test_type::deserialize(buffer, written_bytes);
    REQUIRE(!pVal );
}
// ------------------------------------------------------------------------------------------------
