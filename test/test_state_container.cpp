#include <catch2/catch.hpp>

#include "device/state_container.h"

using namespace framework;
// ------------------------------------------------------------------------------------------------
TEST_CASE("StateContainer", "[ErrorCode]")
{
    SECTION("No error")
    {
        StateContainer states(ut_ecode::create(0, 0));
        REQUIRE(!states.hasError());
        ut_ecode::ptr pEcode = states.getError();
        REQUIRE((bool)pEcode);
        REQUIRE(pEcode->errorCode() == 0);
    }
    SECTION("Error")
    {
        StateContainer states(ut_ecode::create(10, 0));
        REQUIRE(states.hasError());
        ut_ecode::ptr pEcode = states.getError();
        REQUIRE((bool)pEcode);
        REQUIRE(pEcode->errorCode() == 10);
    }

}
// ------------------------------------------------------------------------------------------------
